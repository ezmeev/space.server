import utils.SpaceServer

class EntryPoint {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val server = SpaceServer(8081)

            server.start()
        }
    }
}