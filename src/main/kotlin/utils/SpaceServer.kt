package utils

import server.player.services.*
import java.net.ServerSocket
import java.net.Socket
import java.util.concurrent.Executors

class SpaceServer(val port: Int) {

    private val playersService: PlayersService = PlayersService()

    fun start() {

        println("Server started on $port")

        val socketExecutorService = Executors.newFixedThreadPool(10)

        val serverSocket = ServerSocket(port)

        while (true) {
            val socket = serverSocket.accept()
            socketExecutorService.execute {
                initPlayer(socket)
            }
        }
    }

    private fun initPlayer(socket: Socket) {
        return try {
            playersService.addNewPlayer(Player(ClientSocket(socket), playersService))
            return
        } catch (e: Exception) {
            println("Connection error: [${e.message}]")
            e.printStackTrace()
        }
    }
}