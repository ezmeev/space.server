package utils

import java.net.Socket
import java.security.MessageDigest
import java.util.*
import java.util.regex.Pattern
import javax.xml.bind.DatatypeConverter


class WebSocketHandshaker(val socket: Socket) {

    private val httpRequestDelimiter = "\\r\\n\\r\\n"
    private val secWebSocketKeyPattern = Pattern.compile("Sec-WebSocket-Key: (.*)")
    private val sha1 = MessageDigest.getInstance("SHA-1")

    fun handshake() {
        val inputStream = socket.getInputStream()
        val outputStream = socket.getOutputStream()
        val scanner = Scanner(inputStream, "UTF-8").useDelimiter(httpRequestDelimiter)
        val response = createHandshakeResponse(scanner.next())
        outputStream.write(response, 0, response.size)
    }

    private fun createHandshakeResponse(handshakeRequest: String): ByteArray {
        val get = Pattern.compile("^GET").matcher(handshakeRequest)
        if (get.find()) {
            val match = secWebSocketKeyPattern.matcher(handshakeRequest)
            match.find()
            val webSocketKey = match.group(1) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
            return toByteArray("HTTP/1.1 101 Switching Protocols\r\n"
                    + "Connection: Upgrade\r\n"
                    + "Upgrade: websocket\r\n"
                    + "Sec-WebSocket-Accept: ${encodeResponse(webSocketKey)}"
                    + "\r\n\r\n")
        } else {
            throw IllegalArgumentException("Malformed handshake request: [$handshakeRequest]")
        }
    }

    private fun encodeResponse(webSocketKey: String) =
            DatatypeConverter.printBase64Binary(sha1.digest(toByteArray(webSocketKey)))

    private fun toByteArray(string: String) = string.toByteArray(charset("UTF-8"))
}