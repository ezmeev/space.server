package utils

import java.util.Random


class RandomUtils {

    companion object {
        private val alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        private val rnd = Random()

        fun rnd(): String {
            val salt = StringBuilder()
            while (salt.length < 8) {
                val index = (rnd.nextFloat() * alphabet.length).toInt()
                salt.append(alphabet[index])
            }
            return salt.toString()
        }
    }

}