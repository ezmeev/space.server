package utils

enum class OpCode constructor(var code: Byte) {
    CONTINUOUS(0),
    TEXT(1),
    BINARY(2),
    PING(8),
    PONG(9),
    CLOSING(10);

    companion object {
        fun getOpCode(code: Byte): OpCode =
                enumValues<OpCode>().first { o -> o.code == code }
    }
}
