package server.player.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import server.message.models.MessageType
import server.message.models.TextMessage
import server.message.services.MessageReader
import server.message.services.MessageWriter
import server.player.models.Update
import server.player.models.UpdateData
import utils.WebSocketHandshaker
import java.net.Socket
import java.util.concurrent.Executors

class ClientSocket(private val socket: Socket) {

    private val messageReader: MessageReader = MessageReader(socket.getInputStream())
    private val messageWriter: MessageWriter = MessageWriter(socket.getOutputStream())
    private val playerUpdatesTreadPool = Executors.newSingleThreadExecutor()
    private val jsonMapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

    private lateinit var updatesHandler: ClientUpdatesHandler

    fun init(clientUpdatesHandler:ClientUpdatesHandler): String {
        updatesHandler = clientUpdatesHandler
        val sessionId = initSession()
        subscribeOnClientUpdates()
        return sessionId
    }

    fun sentToClient(update: Update) {
        messageWriter.writeMessage(TextMessage(jsonMapper.writeValueAsString(update)))
    }

    private fun initSession(): String {
        WebSocketHandshaker(socket).handshake()
        val initMessage = messageReader.readMessage()
        if (initMessage.getType() != MessageType.Text) {
            throw IllegalStateException("Wrong init message type: [${initMessage.getType()}]")
        }
        messageWriter.writeMessage(TextMessage("{\"status\": \"ok\"}"))
        return initMessage.getPayload()
    }

    private fun subscribeOnClientUpdates() {
        playerUpdatesTreadPool.submit {
            while (!socket.isClosed) {
                checkClientUpdates()
            }
        }
    }

    private fun checkClientUpdates() {
        try {
            val incomingMessage = messageReader.readMessage()
            if (incomingMessage.getType() == MessageType.Closing) {
                terminate()
                return
            } else if (incomingMessage.getType() == MessageType.Text) {
                onClientUpdate(incomingMessage.getPayload())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun onClientUpdate(message: String) {
        val data = jsonMapper.readValue<UpdateData>(message)
        data.rawMessage = message
        updatesHandler.onClientMessage(data)
    }

    private fun terminate() {
        updatesHandler.onTerminate()
        socket.close()
    }
}