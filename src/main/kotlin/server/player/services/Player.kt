package server.player.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import server.player.models.*

class Player(private val clientSocket: ClientSocket,
             private val playersService: PlayersService)
    : Comparable<Player>, ServerUpdatesHandler, ClientUpdatesHandler {

    private val jsonMapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())
    private var playerState: PlayerState = PlayerStateHelper.create(clientSocket.init(this))

    init {
        sendExistingPlayersToClient()
    }

    fun getPlayerState(): PlayerState {
        return playerState
    }

    fun getSessionId(): String {
        return playerState.sessionId
    }

    override fun onServerUpdate(update: Update) {
//        println("Client <- $update")
        clientSocket.sentToClient(update)
    }

    override fun onTerminate() {
        playersService.removePlayer(this)
    }

    override fun onClientMessage(updateData: UpdateData) {
//        println("Client -> $updateData")
        when (updateData.command.type) {
            CommandType.RocketFired -> {
                val update: RocketUpdate = jsonMapper.readValue(updateData.rawMessage)
                playersService.rocketFired(update)
            }
            CommandType.PlayerExit -> {
                val update: PlayerUpdate = jsonMapper.readValue(updateData.rawMessage)
                playersService.removePlayer(this)
                playersService.sendUpdateToPlayers(update)

            }
            CommandType.PlayerDestroyed -> {
                val update: PlayerUpdate = jsonMapper.readValue(updateData.rawMessage)
                playerState.alive = update.player.alive
                playersService.sendUpdateToPlayers(update)

            }
            CommandType.PlayerLocationUpdate -> {
                val update: PlayerUpdate = jsonMapper.readValue(updateData.rawMessage)
                playerState.x = update.player.x
                playerState.y = update.player.y
                playerState.angle = update.player.angle
                playersService.sendUpdateToPlayers(update)
            }
            else -> {
                val update: PlayerUpdate = jsonMapper.readValue(updateData.rawMessage)
                playersService.sendUpdateToPlayers(update)
            }
        }

    }

    private fun sendExistingPlayersToClient() {
        playersService.getPlayers().forEach { p ->
            clientSocket.sentToClient(PlayerUpdate(p.playerState, Command(CommandType.ExistingPlayer)))
        }
    }

    override fun compareTo(other: Player): Int {
        return this.hashCode().compareTo(other.hashCode())
    }
}