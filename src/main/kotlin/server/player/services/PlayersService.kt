package server.player.services

import server.player.models.*
import java.util.concurrent.*

class PlayersService {

    private val players: ConcurrentSkipListSet<Player> = ConcurrentSkipListSet()
    private val executor: ExecutorService = Executors.newCachedThreadPool()

    fun addNewPlayer(player: Player) {
        players.add(player)
        sendUpdateToPlayers(PlayerUpdate(player.getPlayerState(), Command(CommandType.NewPlayer)))
    }

    fun removePlayer(player: Player) {
        players.remove(player)
    }

    fun sendUpdateToPlayers(update: Update) {
        val stalePlayers = arrayListOf<Player>()
        players.forEach { p ->
            try {
                p.onServerUpdate(update)
            } catch (e: Exception) {
                stalePlayers.add(p)
                println("Will remove player [${p.getSessionId()}] because of exception: [${e.localizedMessage}]")
            }
        }

        stalePlayers.forEach(this::removePlayer)
    }

    fun getPlayers(): Set<Player> {
        return players
    }

    fun rocketFired(update: RocketUpdate) {
        executor.submit({
            var ticks = 0
            val maxTicks = 30
            val delta = 15
            val player = update.player
            val rocket = update.rocket

            sendUpdateToPlayers(RocketUpdate(player, rocket, Command(CommandType.RocketFired)))

            while (ticks++ < maxTicks) {
                rocket.x += (delta * Math.cos(rocket.angle.toDouble() * Math.PI / 180)).toInt()
                rocket.y -= (delta * Math.sin(rocket.angle.toDouble() * Math.PI / 180)).toInt()
                sendUpdateToPlayers(RocketUpdate(player, rocket, Command(CommandType.RocketLocationUpdate)))
                Thread.sleep(10)
            }

            sendUpdateToPlayers(RocketUpdate(player, rocket, Command(CommandType.RocketExplosion)))
        })
    }
}