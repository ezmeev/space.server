package server.player.services

import server.player.models.Update

interface ServerUpdatesHandler {
    fun onServerUpdate(update: Update)
}