package server.player.services

import server.player.models.PlayerState
import java.util.*

class PlayerStateHelper {
    companion object {

        fun create(sessionId: String): PlayerState {
            val random = Random()
            val x = random.nextInt(200)
            val y = random.nextInt(200)
            return PlayerState(sessionId, x, y)
        }
    }
}