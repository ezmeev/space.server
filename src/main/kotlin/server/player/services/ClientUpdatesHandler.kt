package server.player.services

import server.player.models.UpdateData

interface ClientUpdatesHandler {
    fun onTerminate()
    fun onClientMessage(updateData: UpdateData)
}