package server.player.models

data class RocketState(val id: String, var x: Int = 0, var y: Int = 0, var angle: Int = 0)