package server.player.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UpdateData(override val command: Command) : Update {
    lateinit var rawMessage: String
}