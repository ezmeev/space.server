package server.player.models

data class RocketUpdate(val player: PlayerState, val rocket: RocketState, override val command: Command): Update