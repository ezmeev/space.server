package server.player.models

data class PlayerState(val sessionId: String, var x: Int = 0, var y: Int = 0, var angle: Int = 90, var alive: Boolean = true)