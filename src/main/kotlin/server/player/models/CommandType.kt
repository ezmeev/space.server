package server.player.models

enum class CommandType {
    NewPlayer,
    ExistingPlayer,
    PlayerLocationUpdate,
    RocketLocationUpdate,
    RocketExplosion,
    RocketFired,
    PlayerDestroyed,
    PlayerExit
}