package server.message.models

class PingMessage(private val payload: String) : Message {
    override fun getPayload(): String {
        return payload
    }

    override fun getType(): MessageType {
        return MessageType.Ping
    }
}