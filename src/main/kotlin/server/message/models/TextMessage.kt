package server.message.models

data class TextMessage(private val payload: String) : Message {
    override fun getPayload(): String {
        return payload
    }

    override fun getType(): MessageType {
        return MessageType.Text
    }
}