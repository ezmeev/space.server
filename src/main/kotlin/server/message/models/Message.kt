package server.message.models

interface Message {
    fun getType(): MessageType
    fun getPayload(): String
}