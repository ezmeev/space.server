package server.message.models

class DeadMessage: Message {
    override fun getPayload(): String {
        return ""
    }

    override fun getType(): MessageType {
        return MessageType.Closing
    }
}