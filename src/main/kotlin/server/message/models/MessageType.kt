package server.message.models


enum class MessageType {
    Closing,
    Text,
    Ping,
    Pong,
    Binary,
    Continuous
}