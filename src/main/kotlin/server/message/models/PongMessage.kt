package server.message.models

class PongMessage : Message {
    override fun getPayload(): String {
        return ""
    }

    override fun getType(): MessageType {
        return MessageType.Pong
    }
}