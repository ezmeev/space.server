package server.message.services

import utils.ByteUtils
import utils.OpCode
import server.message.models.DeadMessage
import server.message.models.Message
import server.message.models.PingMessage
import server.message.models.TextMessage
import java.io.InputStream
import java.math.BigInteger
import java.nio.ByteBuffer
import java.util.*
import kotlin.experimental.xor

class MessageReader(private val input: InputStream) {

    fun readMessage(): Message {
        val b1 = input.read()
        if (b1 == -1) {
            return DeadMessage()
        }

        val opCode = ByteUtils.toOpCode(b1.toByte())
        return when (opCode) {
            OpCode.TEXT -> TextMessage(readPayload())
            OpCode.PING -> PingMessage(readPayload())
            OpCode.CLOSING -> DeadMessage()
            else -> throw IllegalStateException("Unsupported OpCode: [$opCode]")
        }
    }

    private fun readPayload(): String {
        val b2 = input.read().toByte()
        val length = getLength(b2)
        val maskKey = getMaskKey(b2)
        return String(readPayload(input, maskKey, length), charset("UTF-8"))
    }

    private fun getLength(b2: Byte): Int {
        val length = ByteUtils.toLength(b2).toInt()
        if (length <= 125)
            return length

        if (length == 126) {
            val buffer = ByteArray(3)
            buffer[1] = input.read().toByte()
            buffer[2] = input.read().toByte()
            return BigInteger(buffer).intValueExact()
        }

        if (length == 127) {
            val buffer = ByteArray(8)
            input.read(buffer)
            return BigInteger(buffer).intValueExact()
        }
        throw RuntimeException("Illegal length: $length")
    }

    private fun getMaskKey(b2: Byte): ByteArray? {
        var maskKey: ByteArray? = null
        if (ByteUtils.masked(b2)) {
            maskKey = ByteArray(4)
            input.read(maskKey)
        }
        return maskKey
    }

    private fun readPayload(inStream: InputStream, maskKey: ByteArray?, payloadLength: Int): ByteArray {
        val data = ByteArray(payloadLength)
        if (inStream.read(data) != data.size)
            throw IllegalStateException("Didn't read all the bytes.")

        if (maskKey != null) {
            for (i in 0 until payloadLength) {
                data[i] = data[i] xor maskKey[i % 4]
            }
        }
        return data
    }
}