package server.message.services

import utils.OpCode
import server.message.models.Message
import server.message.models.MessageType
import java.io.IOException
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*
import kotlin.experimental.or
import kotlin.experimental.xor


class MessageWriter(private val output: OutputStream) {

    fun writeMessage(message: Message) {
        val data = createFrame(toOpCode(message.getType()), message.getPayload())
        try {
            output.write(data)
            output.flush()
        } catch (e: IOException) {
            throw IllegalStateException(e)
        }
    }

    private fun toOpCode(type: MessageType): OpCode {
        return when (type){
            MessageType.Closing -> OpCode.CLOSING
            MessageType.Text -> OpCode.TEXT
            MessageType.Ping -> OpCode.PING
            MessageType.Pong -> OpCode.PONG
            MessageType.Binary -> OpCode.BINARY
            MessageType.Continuous -> OpCode.CONTINUOUS
        }

    }

    private fun createFrame(code: OpCode, payloadString: String): ByteArray {
        val payload = payloadString.toByteArray(charset("UTF-8"))

        val mask = false
        val sizeInBytes = if (payload.size <= 125) 1 else if (payload.size <= 65535) 2 else 8

        val buf = ByteBuffer.allocate(1 + (if (sizeInBytes > 1) sizeInBytes + 1 else sizeInBytes) + (if (mask) 4 else 0) + payload.size)

        var one = (-128).toByte()
        one = one or code.code
        buf.put(one)

        val payloadlengthbytes = toByteArray(payload.size, sizeInBytes)
        assert(payloadlengthbytes.size == sizeInBytes)

        if (sizeInBytes == 1) {
            buf.put(payloadlengthbytes[0] or if (mask) (-128).toByte() else 0)
        } else if (sizeInBytes == 2) {
            buf.put(126.toByte() or if (mask) (-128).toByte() else 0)
            buf.put(payloadlengthbytes)
        } else if (sizeInBytes == 8) {
            buf.put(127.toByte() or if (mask) (-128).toByte() else 0)
            buf.put(payloadlengthbytes)
        } else {
            throw RuntimeException("Size representation not supported/specified")
        }

        if (mask) {
            val maskKey = ByteBuffer.allocate(4)
            maskKey.putInt(Random().nextInt())
            buf.put(maskKey.array())
            for (i in payload.indices) {
                buf.put(payload[i] xor maskKey.get(i % 4))
            }
        } else {
            buf.put(payload)
        }

        buf.flip()

        return buf.array()
    }

    private fun toByteArray(value: Int, byteCount: Int): ByteArray {
        val buffer = ByteArray(byteCount)
        val highest = 8 * byteCount - 8
        for (i in 0 until byteCount) {
            buffer[i] = value.ushr(highest - 8 * i).toByte()
        }
        return buffer
    }
}