package utils

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner

@RunWith(BlockJUnit4ClassRunner::class)
class RandomUtilsTest {

    @Test
    fun rnd() {
        val guid = RandomUtils.rnd()
        Assert.assertNotNull(guid)
        Assert.assertEquals(8, guid.length)
    }
}